<?php

/**
 * Autoload for the project
 * @global array $CONFIG
 * 
 * @param type $class
 */
function app_autoloader($class){
    global $CONFIG;

    $baseDirectories = array(
        'C248_A4_2018F' => $CONFIG["base_dir"].DIRECTORY_SEPARATOR.'dev-common.voicemeup.com'.DIRECTORY_SEPARATOR.'php'.DIRECTORY_SEPARATOR.'class',
    );

    // Split the namespace on the last backslash and get the two parts
    $classComponents = explode("\\", strrev($class), 2);
    $className = strrev($classComponents[0]);
    $namespacePrefix = strrev($classComponents[1]);

    if ($baseDirectories[$namespacePrefix]){
        $filename = $baseDirectories[$namespacePrefix].DIRECTORY_SEPARATOR.$className.'.php';
        if (file_exists($filename)){
            require $filename;
        }
    }
}