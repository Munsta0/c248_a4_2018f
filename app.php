<?php
// Get the current working directory
$CONFIG["base_dir"] = dirname(__FILE__);

//Load our autoloader function
require_once 'autoloader.php';

// Register our autoload function so it is called when a missing class is found
spl_autoload_register('app_autoloader', false);

$gardenGame;