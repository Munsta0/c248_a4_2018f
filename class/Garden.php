<?php

namespace C248_A4_2018F;

/**
 * @author jbducharme
 */
class Garden
{

    const FLOWER = 'f';
    const TREE = 't';
    const NOTHING = '-';
    const DEFAULT_GARDEN_SIZE = 3;

    /**
     * Our 2D array representation of our garden
     * @var string[][]
     */
    protected $garden;

    /**
     * PHP Doesn't support multiple construct, need to make the variables optional instead
     *
     * @param int $gardenSize
     */
    public function __construct($gardenSize = null)
    {
        if (is_null($gardenSize)) {
            // If size is not passed, create a default size garden
            $this->initializeGarden(DEFAULT_GARDEN_SIZE);
        } else {
            // If size is passed, created a garden of the requested size
            $this->initializeGarden($gardenSize);
        }
    }

    /**
     * Create the initial garden with the requested size
     * 
     * @param int $gardenSize
     */
    private function initializeGarden($gardenSize)
    {
        // Create one column of the required size
        $col = array_fill(0, $gardenSize, Garden::NOTHING);

        // Fill each row with an empty column #magic
        $this->garden(0, $gardenSize, $col);
    }

    /**
     * Return the string contained at the requested position
     *
     * @param int $row
     * @param int $col
     */
    public function getInLocation($row, $col)
    {
        return $this->garden[$row][$col];
    }

    /**
     * Set requested location as flower
     * @param int $row
     * @param int $col
     */
    public function plantFlower($row, $col)
    {
        $this->garden[$row][$col] = Garden::FLOWER;
    }

    /**
     * Set requested location as tree. Since tree is 2x2, $row+1 $col, $row $col+1 and $row+1 $col+1 will also be set as tree
     *
     * @param int $row
     * @param int $col
     */
    public function plantTree($row, $col)
    {
        $this->garden[$row][$col] = Garden::TREE;
        $this->garden[$row + 1][$col] = Garden::TREE;
        $this->garden[$row][$col + 1] = Garden::TREE;
        $this->garden[$row + 1][$col + 1] = Garden::TREE;
    }

    /**
     * Remove a flower at the requested location. This really should have been called "removePlant"
     *
     * @param int $row
     * @param int $col
     */
    public function removeFlower($row, $col)
    {
        $this->garden[$row][$col] = Garden::NOTHING;
    }

    /**
     * Check how many 2x2 trees can fit in the garden
     *
     * @return int
     */
    public function countPossibleTrees()
    {
        $numberOfTrees = 0;
        // Skip the last row since it's impossible to fit it there
        for ($rowCount = 0; $rowCount < count($this->garden) - 1; $rowCount++) {
            for ($colCount = 0; $colCount < count($this->garden[0]) - 1; $colCount++) {
                if ($this->canTreeFit($rowCount, $colCount)) {
                    $numberOfTrees++;
                }
            }
        }

        return $numberOfTrees;
    }

    /**
     * Check how many 2x2 trees can fit in the garden
     *
     * @return int
     */
    public function countPossibleFlowers()
    {
        return $this->countInstance(Garden::FLOWER);
    }

    /**
     * Find how many instance of a string is in the garden ( complete match, doesn't support partial match )
     *
     * @param string $string
     *
     * @return int
     */
    private function countInstance($string)
    {
        $rawMatches = [];
        for ($count = 0; count($this->garden); $count++) {
            // Find all the matches on each row
            $rawMatches[] = array_count_values($this->garden[$count]);
        }

        // Get all the totals from each row them add them up
        // TODO : check what happens when there is nothing found, it's possible some validation is needed here
        return array_sum(array_column($rawMatches, $string));
    }

    /**
     * Determine if a tree can fit in the requested location
     *
     * @param int $row
     * @param int $col
     *
     * @return boolean
     */
    protected function canTreeFit($row, $col)
    {
        return $this->garden[$row][$col] == Garden::NOTHING &&
            $this->garden[$row + 1][$col] == Garden::NOTHING &&
            $this->garden[$row][$col + 1] == Garden::NOTHING &&
            $this->garden[$row + 1][$col + 1] == Garden::NOTHING;
    }

    /**
     * Determine if the garden is full. Should have been called isGardenFull, since it returns a boolean.
     *
     * @return boolean
     */
    public function gardenFull()
    {
        return $this->countInstance(Garden::NOTHING) === 0;
    }

    /**
     * This is a magic PHP method that is called whenever we try to cast this object as a string
     *
     * @return string
     */
    public function __toString()
    {
        // Execute the implode on each row of the garden and return the result
        return array_reduce($this->garden, function(&$carry, $item) {
            // Add a line return if this is not the first row
            if ($carry != "") {
                $carry .= "\n";
            }
            // Convert the entire coloumn in a f , t , ... string
            $carry .= implode(" , ", $item);
        });
    }

}
