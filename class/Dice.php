<?php

namespace C248_A4_2018F;

/**
 * @author jbducharme
 */
class Dice
{

    /**
     *
     * @var int
     */
    protected $firstDice;

    /**
     *
     * @var int
     */
    protected $secondDice;

    /**
     *
     */
    public function __construct()
    {
        $this->firstDice = 0;
        $this->secondDice = 0;
    }

    /**
     * Return value of the first dice
     *
     * @return int
     */
    public function getFirstDice()
    {
        return $this->firstDice;
    }

    /**
     * Return value of the second dice
     *
     * @return int
     */
    public function getSecondDice()
    {
        return $this->secondDice;
    }

    /**
     * Roll both dices between 1 and 6 then return the combined value
     *
     * @return int
     */
    public function rollDice()
    {
        $this->firstDice = rand(1, 6);
        $this->secondDice = rand(1, 6);

        return $this->firstDice + $this->secondDice;
    }

    /**
     * This is a magic PHP method that is called whenever we try to cast this object as a string
     *
     * @return string
     */
    public function __toString()
    {
        return "First dice : {$this->firstDice} - Second dice : {$this->secondDice}";
    }

}
